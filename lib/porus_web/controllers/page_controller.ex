defmodule PorusWeb.PageController do
  use PorusWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
