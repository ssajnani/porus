defmodule Porus.Repo do
  use Ecto.Repo,
    otp_app: :porus,
    adapter: Ecto.Adapters.Postgres
end
