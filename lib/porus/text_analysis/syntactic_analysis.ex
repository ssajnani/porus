defmodule Porus.TextAnalysis.SyntacticAnalysis do
  use GenServer
  
  def init(data) do
    {:ok, data}
  end

  def start(id) do
    GenServer.start_link(Porus.TextAnalysis.SyntacticAnalysis, %{}, name: id)
  end

  def handle_call({:analyze, text}, _from, state) do
    with result <- GcNLP.annotate_text text
      do
        {:reply, result, result}
      else
        err -> {:reply, %{"error" => "Call to Google Natural Language API failed."}, %{}}
      end
  end

  def handle_call({:get_sentiment}, _from, state) do
    case state do
      %{} -> {:error, "No analysis object"}
      _ -> state["documentSentiment"]
    end
  end

  def handle_call({:get_entities}, _from, state) do
    case state do
      %{} -> {:error, "No analysis object"}
      _ -> 
        state["entities"]
        |> Enum.map(&(Task.async(fn -> 
          %{
            "entity" => List.first(&1["mentions"])["text"]["content"],
            "salience" => &1["salience"]
            "type" => List.first(&1["mentions"])["type"]
          } 
        end)))
        |> Enum.map(&Task.await/1)
    end
  end

  def handle_call({:get_entities}, _from, state) do
    case state do
      %{} -> {:error, "No analysis object"}
      _ -> 
        state["entities"]
        |> Enum.map(&(Task.async(fn -> 
          %{
            "entity" => List.first(&1["mentions"])["text"]["content"],
            "salience" => &1["salience"]
            "type" => List.first(&1["mentions"])["type"]
          } 
        end)))
        |> Enum.map(&Task.await/1)
    end
  end

  def  

  def analyze(text) do
    GenServer.call(id, {:analyze, text})  
  end 
end
