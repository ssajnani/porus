# Porus

Generation of a research summary for new projects/tasks. Parse the natural language definitions of title and project description to generate useful google searches for project support.

Currently setup using Google Natural Language API for NLP:

input> GcNLP.annotate_text "There is a lot of new features coming in Elixir 1.4"

output> 
```
%{
  "documentSentiment" => %{"magnitude" => 0.4, "polarity" => 1, "score" => 0.4},
  "entities" => [
    %{
      "mentions" => [
        %{
          "text" => %{"beginOffset" => 11, "content" => "lot"},
          "type" => "COMMON"
        }
      ],
      "metadata" => %{},
      "name" => "lot",
      "salience" => 0.46147496,
      "type" => "OTHER"
    },
    %{
      "mentions" => [
        %{
          "text" => %{"beginOffset" => 22, "content" => "features"},
          "type" => "COMMON"
        }
      ],
      "metadata" => %{},
      "name" => "features",
      "salience" => 0.36635956,
      "type" => "OTHER"
    },
    %{
      "mentions" => [
        %{
          "text" => %{"beginOffset" => 41, "content" => "Elixir 1.4"},
          "type" => "PROPER"
        }
      ],
      "metadata" => %{},
      "name" => "Elixir 1.4",
      "salience" => 0.17216548,
      "type" => "OTHER"
    }
  ],
  "language" => "en",
  "sentences" => [
    %{
      "sentiment" => %{"magnitude" => 0.4, "polarity" => 1, "score" => 0.4},
      "text" => %{
        "beginOffset" => 0,
        "content" => "There is a lot of new features coming in Elixir 1.4"
      }
    }
  ],
  "tokens" => [
    %{
      "dependencyEdge" => %{"headTokenIndex" => 1, "label" => "EXPL"},
      "lemma" => "There",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "DET",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 0, "content" => "There"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 1, "label" => "ROOT"},
      "lemma" => "be",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "INDICATIVE",
        "number" => "SINGULAR",
        "person" => "THIRD",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "VERB",
        "tense" => "PRESENT",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 6, "content" => "is"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 3, "label" => "DET"},
      "lemma" => "a",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "DET",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 9, "content" => "a"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 1, "label" => "NSUBJ"},
      "lemma" => "lot",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "SINGULAR",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "NOUN",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 11, "content" => "lot"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 3, "label" => "PREP"},
      "lemma" => "of",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "ADP",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 15, "content" => "of"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 6, "label" => "AMOD"},
      "lemma" => "new",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "ADJ",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 18, "content" => "new"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 4, "label" => "POBJ"},
      "lemma" => "feature",
      "partOfSpeech" => %{ 
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "PLURAL",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "NOUN",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 22, "content" => "features"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 6, "label" => "VMOD"},
      "lemma" => "come",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "VERB",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 31, "content" => "coming"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 7, "label" => "PREP"},
      "lemma" => "in",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "ADP",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 38, "content" => "in"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 8, "label" => "POBJ"},
      "lemma" => "Elixir",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "SINGULAR",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "NOUN",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 41, "content" => "Elixir"}
    },
    %{
      "dependencyEdge" => %{"headTokenIndex" => 9, "label" => "NUM"},
      "lemma" => "1.4",
      "partOfSpeech" => %{
        "aspect" => "ASPECT_UNKNOWN",
        "case" => "CASE_UNKNOWN",
        "form" => "FORM_UNKNOWN",
        "gender" => "GENDER_UNKNOWN",
        "mood" => "MOOD_UNKNOWN",
        "number" => "NUMBER_UNKNOWN",
        "person" => "PERSON_UNKNOWN",
        "proper" => "PROPER_UNKNOWN",
        "reciprocity" => "RECIPROCITY_UNKNOWN",
        "tag" => "NUM",
        "tense" => "TENSE_UNKNOWN",
        "voice" => "VOICE_UNKNOWN"
      },
      "text" => %{"beginOffset" => 48, "content" => "1.4"}
    }
  ]
}
```

## Timeline:

<s>1) Create CoreNLP Server and make server an Elixir process &#x2612;
    - core-nlp elixir library: https://hexdocs.pm/corenlp/readme.html
    - core-nlp java server: https://stanfordnlp.github.io/CoreNLP/
<br>2) Create a Google SyntaxNext server as an Elixir process &#x2612;
    - https://opensource.google/projects/syntaxnet</s>

<br>1) Compare SyntaxNet, CoreNLP and Spacy  &#x2611;
    - Result: Spacy is better for commercial usage
<br>2) Create a api server that holds the dispacy and sense2vec functions by spacy (https://github.com/explosion/spacy-services) and the gpt-2 text generation function (https://github.com/openai/gpt-2) &#x2611;
</font>
<br>3) Create search queries (design WIP): http://www.bowdoin.edu/~allen/nlp/nlp1.html
<br>4) Scrape data from queried pages (design WIP)

## Development Environment

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
