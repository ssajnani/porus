# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :porus,
  ecto_repos: [Porus.Repo]

config :goth, json: "config/gcloud-secret.json" |> File.read!

# Configures the endpoint
config :porus, PorusWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XoLXLDRgPTEKap8YwMnVjx4wjoMehiBj823kKutFW7WQTEE7z1TVXoP52kYlSjLK",
  render_errors: [view: PorusWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Porus.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
